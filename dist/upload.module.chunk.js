webpackJsonp(["upload.module"],{

/***/ "./node_modules/ngx-show-hide-password/ngx-show-hide-password.umd.js":
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
	 true ? factory(exports, __webpack_require__("./node_modules/@angular/core/esm5/core.js"), __webpack_require__("./node_modules/@angular/forms/esm5/forms.js"), __webpack_require__("./node_modules/@angular/common/esm5/common.js")) :
	typeof define === 'function' && define.amd ? define(['exports', '@angular/core', '@angular/forms', '@angular/common'], factory) :
	(factory((global['ngx-show-hide-password'] = {}),global.core,global.forms,global.common));
}(this, (function (exports,core,forms,common) { 'use strict';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
/**
 * Add a split input button to password or text input. Toggles input type between "text" and "password".
 *
 * \@example
 * <show-hide-password size="sm|lg">
 * <input type="password" name=... />
 * </show-hide-password>
 */
var ShowHidePasswordComponent = /** @class */ (function () {
    function ShowHidePasswordComponent(elem, renderer) {
        this.elem = elem;
        this.renderer = renderer;
    }
    /**
     * init component
     */
    /**
     * init component
     * @return {?}
     */
    ShowHidePasswordComponent.prototype.ngOnInit = /**
     * init component
     * @return {?}
     */
    function () {
        this.input = this.elem.nativeElement.querySelector('input');
        if (this.input) {
            this.renderer.addClass(this.elem.nativeElement, 'input-group');
            if (this.size === 'sm') {
                this.renderer.addClass(this.elem.nativeElement, 'input-group-sm');
            }
            else if (this.size === 'lg') {
                this.renderer.addClass(this.elem.nativeElement, 'input-group-lg');
            }
            this.isHidden = this.input.type === 'password';
            this.renderer.addClass(this.input, 'form-control'); // just to be sure
        }
        else {
            throw new Error("No input element found. Please read the docs!");
        }
    };
    /**
     * toggles type of input (text|password)
     * @param {?} $event not used
     * @return {?}
     */
    ShowHidePasswordComponent.prototype.toggleShow = /**
     * toggles type of input (text|password)
     * @param {?} $event not used
     * @return {?}
     */
    function ($event) {
        this.isHidden = !this.isHidden;
        this.renderer.setAttribute(this.input, 'type', this.isHidden ? 'password' : 'text');
    };
    ShowHidePasswordComponent.decorators = [
        { type: core.Component, args: [{
                    selector: 'show-hide-password',
                    template: "\n    <ng-content></ng-content>\n    <div class=\"input-group-append\">\n      <button *ngIf=\"icon\" class=\"btn btn-outline-secondary\" type=\"button\" (click)=\"toggleShow($event)\"\n        [attr.label]=\"isHidden ? 'Show password' : 'Hide password'\" [ngSwitch]=\"icon\">\n        <span *ngSwitchCase=\"'entypo'\" class=\"icon\"\n          [ngClass]=\"{'icon-eye-with-line': !isHidden, 'icon-eye': isHidden}\"\n          [style.font-size]=\"size === 'lg' ? '1.5rem' : ''\"></span>\n        <i class=\"fa fa-fw\" [ngClass]=\"{'fa-eye-slash': !isHidden, 'fa-eye': isHidden, 'fa-lg': size === 'lg'}\"\n          *ngSwitchDefault></i>\n      </button>\n      <div *ngIf=\"!icon\" class=\"input-group-text\">\n        <input type=\"checkbox\" class=\"\" (click)=\"toggleShow($event)\"\n          [attr.label]=\"isHidden ? 'Show password' : 'Hide password'\">\n      </div>\n    </div>\n  "
                },] },
    ];
    /** @nocollapse */
    ShowHidePasswordComponent.ctorParameters = function () { return [
        { type: core.ElementRef, },
        { type: core.Renderer2, },
    ]; };
    ShowHidePasswordComponent.propDecorators = {
        "size": [{ type: core.Input },],
        "icon": [{ type: core.Input },],
    };
    return ShowHidePasswordComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var ShowHidePasswordModule = /** @class */ (function () {
    function ShowHidePasswordModule() {
    }
    /**
     * @return {?}
     */
    ShowHidePasswordModule.forRoot = /**
     * @return {?}
     */
    function () {
        return {
            ngModule: ShowHidePasswordModule
        };
    };
    ShowHidePasswordModule.decorators = [
        { type: core.NgModule, args: [{
                    imports: [common.CommonModule, forms.FormsModule],
                    exports: [ShowHidePasswordComponent],
                    declarations: [ShowHidePasswordComponent]
                },] },
    ];
    /** @nocollapse */
    ShowHidePasswordModule.ctorParameters = function () { return []; };
    return ShowHidePasswordModule;
}());

exports.ShowHidePasswordModule = ShowHidePasswordModule;
exports.ShowHidePasswordComponent = ShowHidePasswordComponent;

Object.defineProperty(exports, '__esModule', { value: true });

})));


/***/ }),

/***/ "./src/app/fileupload.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FileuploadService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FileuploadService = (function () {
    function FileuploadService(_http) {
        this._http = _http;
    }
    FileuploadService.prototype.postFile = function (fileToUpload) {
        var endpoint = 'url';
        var formData = new FormData();
        formData.append('fileKey', fileToUpload, fileToUpload.name);
        console.log(formData);
    };
    FileuploadService.prototype.sendKey = function (superSecretKey) {
        var _this = this;
        this.data = { "superSecretKey": superSecretKey };
        return this._http.post("/api/setUserKey", this.data).map(function (result) { return _this.result = result.json().data; });
    };
    FileuploadService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], FileuploadService);
    return FileuploadService;
}());



/***/ }),

/***/ "./src/app/upload/upload-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__upload_component__ = __webpack_require__("./src/app/upload/upload.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__upload_component__["a" /* UploadComponent */], data: {
            title: 'Upload'
        } }
];
var UploadRoutingModule = (function () {
    function UploadRoutingModule() {
    }
    UploadRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["e" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["e" /* RouterModule */]]
        })
    ], UploadRoutingModule);
    return UploadRoutingModule;
}());



/***/ }),

/***/ "./src/app/upload/upload.component.html":
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container-fluid\">\n  <div class=\"row\">\n    <div class=\"col-sm-12\">\n      <div class=\"card\">\n        <div class=\"card-header\"><h3>Select Files to Upload to Dropbox</h3></div>\n        <div class=\"card-block\">\n          <ng-template #customContent let-c=\"close\" let-d=\"dismiss\">\n          <div class=\"modal-header\">\n              <h4 class=\"modal-title\">Enter Super Secret Key</h4>\n              <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n              <span aria-hidden=\"true\">&times;</span>\n              </button>\n          </div>\n          <div class=\"modal-body\">\n              <show-hide-password size=\"sm\" icon=\"entypo\">\n                  <input type=\"password\" #superSecretKey placeholder=\"Super Secret Key\" name=\"userSecretKey\">\n              </show-hide-password>\n              <button (click)=fileChange(superSecretKey.value)>OK</button>\n              <div *ngIf=\"downloadUrl\" (click)=\"uploadFinal()\"><a href=\"{{downloadUrl.keyDownloadUrl}}\" download>Click to Download Key Image</a></div>\n          </div>\n          <div class=\"modal-footer\">\n              <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"c('Close click')\">Close</button>\n          </div>\n          </ng-template>\n          \n      </div>\n        <div class=\"card-body\" style=\"padding: 20px 20px 20px 20px\">\n          <input type=\"file\" name=\"sampleFile\" ng2FileSelect [uploader]=\"uploader\" (change)=\"openModal(customContent)\">\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/upload/upload.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/upload/upload.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export NgbdModalContent */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__fileupload_service__ = __webpack_require__("./src/app/fileupload.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_file_upload__ = __webpack_require__("./node_modules/ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NgbdModalContent = (function () {
    function NgbdModalContent(activeModal) {
        this.activeModal = activeModal;
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], NgbdModalContent.prototype, "name", void 0);
    NgbdModalContent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'ngbd-modal-content',
            template: "\n  <div class=\"modal-header\">\n    <h4 class=\"modal-title\">Hi there!</h4>\n    <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"activeModal.dismiss('Cross click')\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <p>Hello, {{name}}!</p>\n  </div>\n  <div class=\"modal-footer\">\n    <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"activeModal.close('Close click')\">Close</button>\n  </div>\n"
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["b" /* NgbActiveModal */]])
    ], NgbdModalContent);
    return NgbdModalContent;
}());

var UploadComponent = (function () {
    function UploadComponent(fileUploadService, modalService) {
        this.fileUploadService = fileUploadService;
        this.modalService = modalService;
        this.URL = "/api/uploadDropbox";
        this.uploader = new __WEBPACK_IMPORTED_MODULE_2_ng2_file_upload__["FileUploader"]({ url: this.URL, isHTML5: true });
    }
    UploadComponent.prototype.ngOnInit = function () {
    };
    UploadComponent.prototype.openModal = function (customContent) {
        this.modalService.open(customContent, { windowClass: 'dark-modal' });
    };
    UploadComponent.prototype.openContent = function () {
        var modalRef = this.modalService.open(NgbdModalContent);
        modalRef.componentInstance.name = 'World';
    };
    UploadComponent.prototype.fileChange = function (superSecretKey) {
        var _this = this;
        if (superSecretKey.length != 0) {
            this.fileUploadService.sendKey(superSecretKey).subscribe(function (res) {
                _this.downloadUrl = res;
            });
            console.log(this.uploader);
        }
        else {
            alert("Key cannot be empty");
        }
    };
    UploadComponent.prototype.uploadFinal = function () {
        this.uploader.queue[(this.uploader.queue.length) - 1].upload();
        window.location.reload();
    };
    UploadComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-upload',
            template: __webpack_require__("./src/app/upload/upload.component.html"),
            styles: [__webpack_require__("./src/app/upload/upload.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__fileupload_service__["a" /* FileuploadService */], __WEBPACK_IMPORTED_MODULE_3__ng_bootstrap_ng_bootstrap__["d" /* NgbModal */]])
    ], UploadComponent);
    return UploadComponent;
}());



/***/ }),

/***/ "./src/app/upload/upload.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadModule", function() { return UploadModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_show_hide_password__ = __webpack_require__("./node_modules/ngx-show-hide-password/ngx-show-hide-password.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_show_hide_password___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_show_hide_password__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__upload_routing_module__ = __webpack_require__("./src/app/upload/upload-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__upload_component__ = __webpack_require__("./src/app/upload/upload.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__fileupload_service__ = __webpack_require__("./src/app/fileupload.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_file_upload__ = __webpack_require__("./node_modules/ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_ui_components_module__ = __webpack_require__("./src/app/components/ui-components.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var UploadModule = (function () {
    function UploadModule() {
    }
    UploadModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_3__upload_routing_module__["a" /* UploadRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_7_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_2_ngx_show_hide_password__["ShowHidePasswordModule"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_8__components_ui_components_module__["UIComponentsModule"]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_4__upload_component__["a" /* UploadComponent */]],
            providers: [__WEBPACK_IMPORTED_MODULE_5__angular_http__["c" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_6__fileupload_service__["a" /* FileuploadService */]],
        })
    ], UploadModule);
    return UploadModule;
}());



/***/ })

});
//# sourceMappingURL=upload.module.chunk.js.map