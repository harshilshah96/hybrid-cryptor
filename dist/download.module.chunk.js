webpackJsonp(["download.module"],{

/***/ "./src/app/download/download-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DownloadRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__download_component__ = __webpack_require__("./src/app/download/download.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__download_component__["a" /* DownloadComponent */], data: {
            title: 'Download'
        } }
];
var DownloadRoutingModule = (function () {
    function DownloadRoutingModule() {
    }
    DownloadRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["e" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["e" /* RouterModule */]]
        })
    ], DownloadRoutingModule);
    return DownloadRoutingModule;
}());



/***/ }),

/***/ "./src/app/download/download.component.html":
/***/ (function(module, exports) {

module.exports = "<h2>Download and Decrypt Files</h2>\n<div class=\"container-fluid\">\n  <div *ngFor=\"let file of listOfFiles\" class=\"row\">\n    <div class=\"col-sm-12\" >\n      <div class=\"card\">\n        <div class=\"card-header\" id=\"file.name\" (click)=\"openModal(customContent)\" style=\"cursor: pointer;\">{{file.name}}</div>\n        <ng-template #customContent let-c=\"close\" let-d=\"dismiss\">\n          <div class=\"modal-header\">\n              <h4 class=\"modal-title\">Upload Key Image</h4>\n              <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n              <span aria-hidden=\"true\">&times;</span>\n              </button>\n          </div>\n          <div class=\"modal-body\">\n              <input type=\"file\" name=\"keyFile\" ng2FileSelect [uploader]=\"uploader\" (change)=\"nodeDownloadFile(file.name)\" >\n              <div *ngIf=\"downloadUrl\" (click)=\"refreshPage()\"><a href=\"{{downloadUrl.downloadUrl}}\" download>Click to Download Decrypted File</a></div>\n          </div>\n          <div class=\"modal-footer\">\n              <button type=\"button\" class=\"btn btn-secondary btn-raised\" (click)=\"c('Close click')\">Close</button>\n          </div>\n          </ng-template>\n      </div>\n    </div>  \n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/download/download.component.scss":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/download/download.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DownloadComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__downloaddropbox_service__ = __webpack_require__("./src/app/downloaddropbox.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__("./node_modules/@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload__ = __webpack_require__("./node_modules/ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ng2_file_upload__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DownloadComponent = (function () {
    function DownloadComponent(downloadDropboxService, modalService) {
        var _this = this;
        this.downloadDropboxService = downloadDropboxService;
        this.modalService = modalService;
        this.URL = "/api/checkUserKey";
        this.uploader = new __WEBPACK_IMPORTED_MODULE_3_ng2_file_upload__["FileUploader"]({ url: this.URL, isHTML5: true });
        console.log("For breakpoint");
        //breakpoint
        this.downloadDropboxService.getFolders()
            .subscribe(function (res) {
            _this.listOfFiles = res.entries;
            console.log(_this.listOfFiles[0].name);
        });
        console.log("Download component reaching here");
        console.log("Folders " + this.listOfFiles);
    }
    DownloadComponent.prototype.ngOnInit = function () {
    };
    DownloadComponent.prototype.nodeDownloadFile = function (fileName) {
        var _this = this;
        console.log("Calling nodeDownloadFile method");
        console.log(this.uploader.queue.length);
        this.uploader.queue[(this.uploader.queue.length) - 1].upload();
        this.downloadDropboxService.nodeDownloadZip(fileName)
            .subscribe(function (res) {
            if (res.downloadUrl != "DecryptionError")
                _this.downloadUrl = res;
            else
                alert("Decryption Error. Provide correct key or contact system admin");
        });
    };
    DownloadComponent.prototype.openModal = function (customContent) {
        this.modalService.open(customContent, { windowClass: 'dark-modal' });
    };
    DownloadComponent.prototype.refreshPage = function () {
        window.location.reload();
    };
    DownloadComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-download',
            template: __webpack_require__("./src/app/download/download.component.html"),
            styles: [__webpack_require__("./src/app/download/download.component.scss")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__downloaddropbox_service__["a" /* DownloaddropboxService */], __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["d" /* NgbModal */]])
    ], DownloadComponent);
    return DownloadComponent;
}());



/***/ }),

/***/ "./src/app/download/download.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DownloadModule", function() { return DownloadModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__downloaddropbox_service__ = __webpack_require__("./src/app/downloaddropbox.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__download_routing_module__ = __webpack_require__("./src/app/download/download-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__download_component__ = __webpack_require__("./src/app/download/download.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_file_upload__ = __webpack_require__("./node_modules/ng2-file-upload/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_file_upload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_ng2_file_upload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_ui_components_module__ = __webpack_require__("./src/app/components/ui-components.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var DownloadModule = (function () {
    function DownloadModule() {
    }
    DownloadModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_4__download_routing_module__["a" /* DownloadRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_6_ng2_file_upload__["FileUploadModule"],
                __WEBPACK_IMPORTED_MODULE_7__components_ui_components_module__["UIComponentsModule"]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_5__download_component__["a" /* DownloadComponent */]],
            providers: [__WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* HttpModule */], __WEBPACK_IMPORTED_MODULE_3__downloaddropbox_service__["a" /* DownloaddropboxService */]]
        })
    ], DownloadModule);
    return DownloadModule;
}());



/***/ }),

/***/ "./src/app/downloaddropbox.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DownloaddropboxService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("./node_modules/@angular/http/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DownloaddropboxService = (function () {
    function DownloaddropboxService(_http) {
        this._http = _http;
    }
    DownloaddropboxService.prototype.getFolders = function () {
        var _this = this;
        console.log("Download Dropbox Service called");
        return this._http.get('/api/getDropboxFiles').map(function (result) { return _this.result = result.json().data; });
    };
    DownloaddropboxService.prototype.nodeDownloadZip = function (downloadFile) {
        var _this = this;
        console.log("Node download called within service");
        this.download = { "downloadFile": downloadFile };
        return this._http.post("/api/downloadDropboxFolder", this.download).map(function (result) { return _this.result = result.json().data; });
    };
    DownloaddropboxService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], DownloaddropboxService);
    return DownloaddropboxService;
}());



/***/ })

});
//# sourceMappingURL=download.module.chunk.js.map