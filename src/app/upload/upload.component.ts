import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import {FileuploadService} from '../fileupload.service';
import {FileUploader} from 'ng2-file-upload';
import {NgClass, NgStyle} from '@angular/common';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngbd-modal-content',
  template: `
  <div class="modal-header">
    <h4 class="modal-title">Hi there!</h4>
    <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p>Hello, {{name}}!</p>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary btn-raised" (click)="activeModal.close('Close click')">Close</button>
  </div>
`
})

export class NgbdModalContent {
  @Input() name;
  constructor(public activeModal: NgbActiveModal) { }
}

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  
  enc1stat: any;
  enc2stat: any;
  enc3stat: any;
  downloadUrl: any;
  URL: any="/api/uploadDropbox";
  uploader: FileUploader= new FileUploader({url:this.URL, isHTML5: true});
  constructor(public fileUploadService: FileuploadService, private modalService: NgbModal) {
    
   }

  ngOnInit() {
    
  }

  openModal(customContent) {
    this.modalService.open(customContent, { windowClass: 'dark-modal' });
}
  openContent() {
    const modalRef = this.modalService.open(NgbdModalContent);
    modalRef.componentInstance.name = 'World';
}

  fileChange(superSecretKey){
    if(superSecretKey.length!=0){
      this.fileUploadService.sendKey(superSecretKey).subscribe(res => {
      this.downloadUrl=res;  
      });
      console.log(this.uploader);
      
    }
    else{
      alert("Key cannot be empty");
    }
  }
  uploadFinal(){
    this.uploader.queue[(this.uploader.queue.length)-1].upload();
    window.location.reload();
  }
  

}
