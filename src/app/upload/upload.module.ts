import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalsComponent, NgbdModalContent } from "../components/bootstrap/modals/modals.component";
import { ShowHidePasswordModule } from 'ngx-show-hide-password';
import { UploadRoutingModule } from './upload-routing.module';
import { UploadComponent } from './upload.component';
import { HttpModule } from '@angular/http';
import { FileuploadService } from '../fileupload.service';
import {FileUploadModule} from 'ng2-file-upload';
import { UIComponentsModule } from '../components/ui-components.module';

@NgModule({
  imports: [
    CommonModule,
    UploadRoutingModule,
    HttpModule,
    FileUploadModule,
    ShowHidePasswordModule.forRoot(),
    UIComponentsModule
  ],
  declarations: [UploadComponent],
  providers: [HttpModule, FileuploadService],
})
export class UploadModule { }
