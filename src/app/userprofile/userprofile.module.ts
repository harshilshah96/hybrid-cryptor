import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserprofileRoutingModule } from './userprofile-routing.module';
import { UserprofileComponent } from './userprofile.component';
import { HttpModule } from '@angular/http';
import { NodeapiService } from '../nodeapi.service';

@NgModule({
  imports: [
    CommonModule,
    UserprofileRoutingModule,
    HttpModule
  ],
  declarations: [UserprofileComponent],
  providers: [HttpModule,NodeapiService]
})
export class UserprofileModule { }
