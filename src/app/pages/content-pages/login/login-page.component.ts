import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import {NodeapiService} from '../../../nodeapi.service';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent {

    @ViewChild('f') loginForm: NgForm;

    constructor(private router: Router,
        private route: ActivatedRoute, public nodeapiService: NodeapiService) { }
    
    onClickLogin(){
    this.nodeapiService.nodeLogin().subscribe(res => {window.location.href=res.url});
    }      
    
}