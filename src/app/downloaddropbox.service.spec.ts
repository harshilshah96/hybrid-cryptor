import { TestBed, inject } from '@angular/core/testing';

import { DownloaddropboxService } from './downloaddropbox.service';

describe('DownloaddropboxService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DownloaddropboxService]
    });
  });

  it('should be created', inject([DownloaddropboxService], (service: DownloaddropboxService) => {
    expect(service).toBeTruthy();
  }));
});
