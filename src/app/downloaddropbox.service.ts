import { Injectable } from '@angular/core';
import {HttpModule, Headers, RequestOptions, Http} from '@angular/http'; 
@Injectable()
export class DownloaddropboxService {
  result: any;
  download: any;
  constructor(private _http:Http) { }

  getFolders(){
    console.log("Download Dropbox Service called");
    return this._http.get('/api/getDropboxFiles').map(result => this.result=result.json().data);

  }

  nodeDownloadZip(downloadFile: any){
    console.log("Node download called within service");
    this.download={"downloadFile":downloadFile};
    return this._http.post("/api/downloadDropboxFolder",this.download).map(result => this.result=result.json().data);
  }

}
