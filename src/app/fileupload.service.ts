import { Injectable } from '@angular/core';
import {HttpModule, Headers, RequestOptions, Http} from '@angular/http';
@Injectable()
export class FileuploadService {

  data: any;
  result:any
  constructor(private _http: Http) { }

  postFile(fileToUpload: File){
    const endpoint='url';
    const formData: FormData = new FormData();
    formData.append('fileKey',fileToUpload,fileToUpload.name);
    console.log(formData);

  }

  sendKey(superSecretKey){
    this.data={"superSecretKey":superSecretKey};
    return this._http.post("/api/setUserKey",this.data).map(result => this.result=result.json().data);
  }

}
