import { Component } from '@angular/core';
import * as Chartist from 'chartist';
import { ChartType, ChartEvent } from "ng-chartist/dist/chartist.component";
import {NodeapiService} from '../../nodeapi.service';

declare var require: any;

const data: any = require('../../shared/data/chartist.json');

export interface Chart {
    type: ChartType;
    data: Chartist.IChartistData;
    options?: any;
    responsiveOptions?: any;
    events?: ChartEvent;
}

@Component({
    selector: 'app-dashboard1',
    templateUrl: './dashboard1.component.html',
    styleUrls: ['./dashboard1.component.scss']
})

export class Dashboard1Component {
    loggedInUser: any;
    title: any;
    constructor(public nodeapiService: NodeapiService){
        this.nodeapiService.nodeAccessToken().subscribe(res => { console.log(res);this.loggedInUser= res; });
        console.log(this.loggedInUser);
        console.log("Called nodeAccessToken of Service Class from Constructor");
        this.title=window.location.href;

}

    

}
