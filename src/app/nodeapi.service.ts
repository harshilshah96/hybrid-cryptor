import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class NodeapiService {
  result:any;
  user: any;
  constructor(private _http: Http) { }
  nodeLogin(){
    return this._http.get("/api/nodedropboxapi").map(result => this.result=result.json().data);
  }

  nodeAccessToken(){
    console.log("this message is within the service and now calling testdemoapi");
    console.log(window.location.href);
    this.result={"dropurl":window.location.href};  
    return this._http.post("/api/getDropboxAccessToken",this.result).map(result => this.user=result.json().data);
  }

  

}