import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpModule} from '@angular/http';
import {DownloaddropboxService} from '../downloaddropbox.service';
import { DownloadRoutingModule } from './download-routing.module';
import { DownloadComponent } from './download.component';
import {FileUploadModule} from 'ng2-file-upload';
import { ModalsComponent, NgbdModalContent } from "../components/bootstrap/modals/modals.component";
import { UIComponentsModule } from '../components/ui-components.module';
@NgModule({
  imports: [
    CommonModule,
    DownloadRoutingModule,
    HttpModule,
    FileUploadModule,
    UIComponentsModule
  ],
  declarations: [DownloadComponent],
  providers: [HttpModule, DownloaddropboxService]
})
export class DownloadModule { }
