import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import {DownloaddropboxService} from '../downloaddropbox.service';
import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {FileUploader} from 'ng2-file-upload';



@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent implements OnInit {

  listOfFiles:any;
  downloadUrl: any;
  URL: any="/api/checkUserKey";
  uploader: FileUploader= new FileUploader({url:this.URL, isHTML5: true});
  constructor(private downloadDropboxService: DownloaddropboxService, private modalService: NgbModal) {
    console.log("For breakpoint")
    //breakpoint
    this.downloadDropboxService.getFolders()
      .subscribe(
        res => { 
        this.listOfFiles=res.entries;
        console.log(this.listOfFiles[0].name);  
    });
    console.log("Download component reaching here");
    console.log("Folders "+this.listOfFiles);
   }

  ngOnInit() {
  }
  nodeDownloadFile(fileName){
    console.log("Calling nodeDownloadFile method");
    console.log(this.uploader.queue.length);
    this.uploader.queue[(this.uploader.queue.length)-1].upload();
    this.downloadDropboxService.nodeDownloadZip(fileName)
    .subscribe(
      res => {
        if(res.downloadUrl!="DecryptionError")
        this.downloadUrl=res;
        else
        alert("Decryption Error. Provide correct key or contact system admin");
        
    });
  }
  openModal(customContent) {
    this.modalService.open(customContent, { windowClass: 'dark-modal' });
}

refreshPage(){
  window.location.reload();
}

}
