var mongoose=require('mongoose');
var assert=require('assert');
var Schema=mongoose.Schema;
var filesSchema=new Schema({
	userid: {
		type: String, required: true, unique: true
	},
	filename: {
		type:String
	},
	enc1status: {
		type: String
    },
    enc2status: {
        type: String
    },
    enc3status: {
        type: String
    },
    secretKey: {
        type:String
    }
});
var users= mongoose.model('files',filesSchema);
module.exports = users;