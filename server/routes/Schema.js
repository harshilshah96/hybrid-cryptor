var mongoose=require('mongoose');
var assert=require('assert');
var Schema=mongoose.Schema;
var userSchema=new Schema({
	userid: {
		type: String, required: true, unique: true
	},
	name: {
		type:String,
	},
	profilepicurl: {
		type: String
	}
});
var users= mongoose.model('users',userSchema);
module.exports = users;