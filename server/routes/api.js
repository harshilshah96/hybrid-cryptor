const express = require('express');
const router = express.Router();
var mongoose=require('mongoose');
var assert=require('assert');
var users=require('./Schema.js');
var files=require('./fileSchema.js')
var url='mongodb://localhost:27017/hybridcryptor';
var node_dropbox= require('node-dropbox');
mongoose.connect(url);
var db=mongoose.connection;
var url=require('url');
require('isomorphic-fetch');
var Dropbox=require('dropbox').Dropbox;
var multer=require('multer');
var upload= multer({ dest: 'uploads/'})
const fileUpload = require('express-fileupload');
const splitFile=require('split-file');
var encryptor=require('file-encryptor');
var key="SuperSecretKey";
var options = { algorithm: 'blowfish' };
var optionsRC4 = { algorithm: 'RC4'};
var dbx;
var fs=require('fs');
var namesOfFiles;
const steggy=require("steggy");

db.on('error',console.error.bind(console,'Connection Error'));
db.on('open',function(){
    console.log('Connected');
})





// Error handling
const sendError = (err, res) => {
    response.status = 501;
    response.message = typeof err == 'object' ? err.message : err;
    res.status(501).json(response);
};

// Response handling
let response = {
    status: 200,
    data: [],
    message: null
};



router.get('/nodedropboxapi', (req,res) => {
    node_dropbox.Authenticate('l7olxid5wxnuif6','kad3l3ylvnm5f4g','http://localhost:3000/dashboard/dashboard1',function(err, url){
        response.data={'url':url};
        res.json(response);
        console.log(response);
        console.log("Printing after Authenticate api");
    })
});



router.post('/getDropboxAccessToken', (req,res) => {
    console.log("Reaching before calling AccessToken Method");  
    console.log("Reaching here");
    var url_parts=url.parse(req.body.dropurl);
    var params=url_parts.query.split("=");
    console.log(params[1]);
    node_dropbox.AccessToken('l7olxid5wxnuif6','kad3l3ylvnm5f4g',params[1],'http://localhost:3000/dashboard/dashboard1',function(err,body){
        access_token=body.access_token;
        console.log(access_token);
        dbx=new Dropbox({accessToken: access_token});
        dbx.usersGetCurrentAccount().then(function(resp){
            console.log(resp);
            users.find({userid:resp.email},function(err,res){
                if(res.length==0){
                    newuser=new users({userid:resp.email, name:resp.name.display_name, profilepicurl:resp.profile_photo_url});
                    newuser.save(function(err){
                        if(err) throw err;
                        console.log(newuser+" saved");
                    })
                }
                else {
                    console.log("User exists");
                }
            });
            response.data=resp;
            res.json(response);
        }).catch(function(error){
            console.log(error);
        });
    });
});

router.post('/uploadDropbox',function(req,res,next){
    console.log("Reaching here");
    if(!req.files){
        return res.status(400).send("No files were uploaded");
    }
    console.log(req.files);
    let sampleFile=req.files.file;
    console.log(sampleFile);
    let fileName=req.files.file.name;
    sampleFile.mv('./src/assets/'+fileName,function(err){
        if(err){
             return res.status(500).send(err);
        }
        console.log("File Uploaded");
        splitFile.splitFile('./src/assets/'+fileName,3)
            .then((names)=>{
                console.log(names);
                console.log(dbx);
                fs.unlink("./src/assets/"+fileName,(err) => {
                    if(err) throw err;
                    console.log("File deleted");
                });
                dbx.filesCreateFolderV2({path:'/'+fileName}).then((resp) =>{
                    console.log("Folder Creation "+resp);
                    var newfile=new files({userid:users.userid,filename:fileName});
                    newfile.save(function(err){
                        if(err) console.log("File saving to Mongo error: "+err)
                        console.log(fileName+" for ID "+users.userid+" saved");
                    });
                    encryptor.encryptFile(names[0],'./src/assets/enc1.dat',key,options,function(err){
                        if(err) console.log("Error: "+err);
                        console.log("Encryption 1 Complete");
                        enc1=fs.readFile("./src/assets/enc1.dat",function(err,data){
                            if (err) console.log("Reading ec1.dat error");
                            dbx.filesUpload({path:'/'+fileName+'/'+"enc1.dat", contents:data}).then((resp)=>{
                                console.log("Enc 1 dat uploaded");
                                fs.unlink('./src/assets/enc1.dat',(err)=>{
                                    if(err) {console.log("Enc1.dat delete error"+err); throw err;}
                                });
                                files.update({userid:users.userid},{$set:{enc1status:true}},{multi:true},(err)=>{
                                    if(err){console.log("Update enc1status error"+err);}
                                    console.log("Enc1 status updated");
                                })
                            }).catch((err)=>{
                                console.log("Encryption 1 upload error: "+err);
                            });
                        })
                    });
                    encryptor.encryptFile(names[1],'./src/assets/enc2.dat',key,optionsRC4,function(err){
                        if(err) console.log("Error: "+err);
                        console.log("Encryption 2 Complete");
                        var enc2;
                        enc2=fs.readFile("./src/assets/enc2.dat",function(err,data){
                            if (err) console.log("Reading ec2.dat error")
                            dbx.filesUpload({path:'/'+fileName+'/'+"enc2.dat", contents:data}).then((resp)=>{
                                console.log("Enc 2 dat uploaded");
                                fs.unlink('./src/assets/enc2.dat',(err)=>{
                                    if(err) {console.log("Enc2.dat delete error"+err); throw err;}
                                });
                                files.update({userid:users.userid},{$set:{enc2status:true}},{multi:true},(err)=>{
                                    if(err){console.log("Update enc2status error"+err);}
                                    console.log("Enc2 status updated");
                                })
                            }).catch((err)=>{
                                console.log("Encryption 2 upload error: "+err);
                            });
                        })
                    });
                    encryptor.encryptFile(names[2],'./src/assets/enc3.dat',key,function(err){
                        if(err) console.log("Error: "+err);
                        console.log("Encryption 3 Complete");
                        var enc3;
                        enc3=fs.readFile("./src/assets/enc3.dat",function(err,data){
                            if (err) console.log("Reading ec3.dat error")
                            dbx.filesUpload({path:'/'+fileName+'/'+"enc3.dat", contents:data}).then((resp)=>{
                                console.log("Enc 3 dat uploaded");
                                fs.unlink('./src/assets/enc3.dat',(err)=>{
                                    if(err) {console.log("Enc3.dat delete error"+err); throw err;}
                                });
                                files.update({userid:users.userid},{$set:{enc3status:true}},{multi:true},(err)=>{
                                    if(err){console.log("Update enc3status error"+err);}
                                    console.log("Enc3 status updated");
                                })
                            }).catch((err)=>{
                                console.log("Encryption 3 upload error: "+err);
                            });
                        })
                   });
                   console.log("Code to upload encrypted files");
                }).catch((err) => {
                    console.log("Folder creation error"+err);
            });
                
        })
        .catch((err)=>{
            console.log("Error: "+err)
        });
    });
    res.json(response);
});

router.get('/getDropboxFiles',(req,res,next)=>{
    dbx.filesListFolder({path:''}).then((resp)=>{
        console.log(resp);
        response.data=resp;
        res.json(response);
        })
        .catch((err)=>{   
        console.log("Listing Files error"+err);
    });
});

router.post('/downloadDropboxFolder',(req,res,next)=>{
    dbx.filesDownload({path:'/'+req.body.downloadFile+'/enc1.dat'}).then((resp)=>{
        if(resp.fileBinary != undefined){
            fs.mkdir('./src/assets/'+req.body.downloadFile);
            fs.writeFile('./src/assets/'+req.body.downloadFile+'/enc1.dat',resp.fileBinary,
        'binary',(err)=>{
                    if(err)throw err; 
                    console.log("Dropbox file enc1.dat saved");
                }
            );
            dbx.filesDownload({path:'/'+req.body.downloadFile+'/enc2.dat'}).then((resp)=>{
                if(resp.fileBinary != undefined){
                    fs.writeFile('./src/assets/'+req.body.downloadFile+'/enc2.dat',resp.fileBinary,
                    'binary',(err)=>{
                        if(err) throw err;
                        console.log("Dropbox file enc2.dat saved");
                        }
                    );
                    dbx.filesDownload({path:'/'+req.body.downloadFile+'/enc3.dat'})
                    .then((resp)=>{
                        if(resp.fileBinary != undefined){
                            fs.writeFile('./src/assets/'+req.body.downloadFile+'/enc3.dat',resp.fileBinary,
                            'binary',(err)=>{
                                if(err) throw err;
                                console.log("Dropbox file enc3.dat saved");
                                encryptor.decryptFile('./src/assets/'+req.body.downloadFile+'/enc1.dat','./src/assets/'+req.body.downloadFile+'part1',key,options,
                                function(err){
                                    if(err){ 
                                        console.log(err); 
                                        console.log("Decryption Error, please use right key or contact server administrator");
                                        response.data={"downloadUrl":"DecryptionError"};
                                        res.json(response);
                                    }
                                    else {
                                    console.log("Decryption enc1.dat complete");
                                    encryptor.decryptFile('./src/assets/'+req.body.downloadFile+'/enc2.dat','./src/assets/'+req.body.downloadFile+'part2',key,optionsRC4,
                                    function(err){
                                        if(err) {
                                            console.log(err); 
                                            console.log("Decryption Error, please use right key or contact server administrator");
                                            response.data={"downloadUrl":"DecryptionError"};
                                            res.json(response);
                                        }
                                        else {
                                        console.log("Decryption enc2.dat complete");
                                        encryptor.decryptFile('./src/assets/'+req.body.downloadFile+'/enc3.dat','./src/assets/'+req.body.downloadFile+'part3',key,
                                        function(err){
                                                if(err) {
                                                    console.log(err); 
                                                    console.log("Decryption Error, please use right key or contact server administrator");
                                                    response.data={"downloadUrl":"DecryptionError"};
                                                    res.json(response);
                                                } 
                                                else{
                                                console.log("Decryption enc3.dat complete");
                                                namesOfFiles=["./src/assets/"+req.body.downloadFile+"part1","./src/assets/"+req.body.downloadFile+"part2","./src/assets/"+req.body.downloadFile+"part3"];
                                                console.log(namesOfFiles);
                                                
                                                splitFile.mergeFiles(namesOfFiles,'./dist/assets/'+req.body.downloadFile.split(' ').join('+')).then((finalFile)=>{
                                                    console.log("Final file recieved");
                                                    response.data={"downloadUrl":"/assets/"+req.body.downloadFile.split(' ').join('+')};
                                                    res.json(response);
                                                    // fs.unlink('./src/assets/'+req.body.downloadFile+"part1",(err)=>{
                                                    //     if(err) {console.log("Enc1.dat delete error"+err); throw err;}
                                                    // });
                                                    // fs.unlink('./src/assets/'+req.body.downloadFile+"part2",(err)=>{
                                                    //     if(err) {console.log("Enc1.dat delete error"+err); throw err;}
                                                    // });
                                                    // fs.unlink('./src/assets/'+req.body.downloadFile+"part3 ",(err)=>{
                                                    //     if(err) {console.log("Enc1.dat delete error"+err); throw err;}
                                                    // });
                                                })
                                                .catch((err)=>{
                                                    console.log(err);
                                                });
                                            }
                                        });
                                    }});
                                }});
                                }
                            );
                        }
                        else {
                            console.log("Resp.fileBinary for enc3.dat undefined");
                        }
                    })
                    .catch((err)=>{
                        console.log("Enc3.dat download error"+err);
                    })
                }
                else{
                    console.log("Resp.fileBinary for enc2.dat undefined");
                }
            })
            .catch((err)=>{
                console.log("Enc2.dat download error"+err);
            })
        }
        else{
            console.log("resp.fileBinary for enc1.dat is undefined");
        }
    })
    .catch((err)=>{
        console.log("Downloading Zip Error"+err);
    });
});

router.post("/setUserKey",(req,res,next)=>{
    key=req.body.superSecretKey;
    console.log(key);
    var original=fs.readFileSync('./src/assets/cryptologo.png');

    var concealed=steggy.conceal()(original,key);
    fs.writeFileSync('./dist/assets/outputfinal.png',concealed);
    response.data={"keyDownloadUrl":'/assets/outputfinal.png'};
    res.json(response);       
});

router.post('/checkUserKey',(req,res,next)=>{
    console.log("Reached check user key");
    if(!req.files){
        return res.status(400).send("No files were uploaded");
    }
    let sampleFile=req.files.file;
    let fileName=req.files.file.name;
    sampleFile.mv('./src/assets/'+fileName,(err)=>{
        if(err){
            console.log(" Key File Upload error"+err);
            throw err;
        }
        else{
            console.log(fileName);
            try
            {var image=fs.readFileSync('./src/assets/'+fileName);
            }catch(err){
                // response={"resp":"Error"}
                // res.json(response);
            }
            var revealed=steggy.reveal()(image);
            console.log(revealed.toString());
            key=revealed.toString();
        }
    });
    
});




module.exports = router;